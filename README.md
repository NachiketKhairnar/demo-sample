## Sample project to perform CRUD operations on Employee

This project provides REST apis to perform CRUD operations on Employee.
It is developed using Spring Boot, Hibernate and Postgres SQL.

## To get the code

Clone the repository:
```
$ git clone https://NachiketKhairnar@bitbucket.org/NachiketKhairnar/demo-sample.git
```

## To run the application
From the command line with Maven:
```
$ cd demo-sample
$ mvn clean install
$ cd demo-sample\target
```

Deploy demo-sample-1.0.war in the app server (e.g. Apache Tomcat).

Access the deployed web application at: http://<hostname>:<port>/demo-sample-1.0
