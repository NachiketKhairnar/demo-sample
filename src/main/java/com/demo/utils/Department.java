package com.demo.utils;

public enum Department {
    FINANCE("Finance"),
    TESTING("Testing");

    String deptName;
    Department(String finance) {
        this.deptName = finance;
    }

    public String getDeptName() {
        return deptName;
    }
}
