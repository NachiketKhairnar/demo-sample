package com.demo.service.impl;

import com.demo.model.Employee;
import com.demo.repo.EmployeeRepo;
import com.demo.service.EmployeeService;
import com.demo.utils.Department;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;

/**
 * Service class to perform business operations on employee
 */
@Service
public class EmployeeServiceImpl implements EmployeeService{

    private final Logger LOGGER = LoggerFactory.getLogger(EmployeeServiceImpl.class);

    @Autowired
    EmployeeRepo employeeRepo;

    /**
     * Inserts the employee into db
     * @param employee
     * @return
     */
    @Override
    public Iterable<Employee> addEmployee(List<Employee> employee) {
        LOGGER.trace("Save data");
        return employeeRepo.save(employee);
    }

    /**
     * Deletes employee info from db
     * @param id
     * @return
     */
    @Override
    public boolean deleteEmployee(String id) {
        LOGGER.trace("Delete data");
        employeeRepo.delete(id);
        return true;
    }

    /**
     * Reads the employee data from db
     * @param id
     * @return
     */
    @Override
    public Employee getEmployee(String id) {
        LOGGER.trace("Get data");
        return employeeRepo.findOne(id);
    }

    /**
     * Update the employee info into db
     * @param employee
     * @return
     */
    @Override
    public Employee updateEmployee(Employee employee) {
        LOGGER.trace("Update data");
        return employeeRepo.save(employee);
    }

    /**
     * Generates the billing info for particular department for current month
     * @param department
     * @return
     */
    @Override
    public int generateBilling(String department) {
        LOGGER.trace("Generate billing info");
        int count = employeeRepo.countByDepartment(Department.valueOf(department));
        return count*generateCurrentMonthBill();
    }

    private static int generateCurrentMonthBill() {
        int noOfHours = 8;
        int ratePerHour = 10;
        return Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH)*noOfHours*ratePerHour;
    }
}
