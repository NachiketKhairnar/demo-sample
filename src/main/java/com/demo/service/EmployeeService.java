package com.demo.service;

import com.demo.model.Employee;

import java.util.List;

public interface EmployeeService {

    Iterable<Employee> addEmployee(List<Employee> employee);

    boolean deleteEmployee(String id);

    Employee getEmployee(String id);

    Employee updateEmployee(Employee employee);

    int generateBilling(String department);
}
