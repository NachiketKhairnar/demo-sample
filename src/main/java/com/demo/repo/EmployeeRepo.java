package com.demo.repo;

import com.demo.model.Employee;
import com.demo.utils.Department;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository class to execute queries on "employee" table
 */
@Repository
public interface EmployeeRepo extends CrudRepository<Employee, String>{
    /**
     * Return count of employees by department
     * @param department
     * @return
     */
    int countByDepartment(Department department);
}
