package com.demo.controller;

import com.demo.model.Employee;
import com.demo.service.EmployeeService;
import com.demo.utils.ResponseData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller to perform operations on employee.
 */
@RestController
@RequestMapping(value = "/employees")
public class EmployeeController {

    private final Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);

    @Autowired
    EmployeeService employeeService;

    /**
     * Adds employee info
     * @param employee
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<ResponseData> addEmployee(@RequestBody List<Employee> employee) {
        ResponseEntity<ResponseData> responseEntity;
        ResponseData responseData = new ResponseData();
        try {
            responseData.setStatus("success");
            responseData.setData(employeeService.addEmployee(employee));
            responseData.setMessage("Data saved successfully");
            responseEntity = new ResponseEntity<>(responseData, HttpStatus.OK);
            LOGGER.trace("Data saved");
        } catch(Exception e) {
            LOGGER.error("Error occurred while saving data", e);
            responseData.setStatus("error");
            responseData.setData(e.getMessage());
            responseData.setMessage("Error occurred while saving data");
            responseEntity = new ResponseEntity<>(responseData, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    /**
     * Deletes employee by provided id
     * @param id
     * @return
     */
    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<ResponseData> deleteEmployee(@PathVariable String id) {
        ResponseEntity<ResponseData> responseEntity;
        ResponseData responseData = new ResponseData();
        try {
            responseData.setStatus("success");
            responseData.setData(employeeService.deleteEmployee(id));
            responseData.setMessage("Data deleted successfully");
            responseEntity = new ResponseEntity<>(responseData, HttpStatus.OK);
            LOGGER.trace("Data deleted");
        } catch (Exception e) {
            LOGGER.error("Error occurred while deleting data", e);
            responseData.setStatus("error");
            responseData.setData(e.getMessage());
            responseData.setMessage("Error occurred while deleting data");
            responseEntity = new ResponseEntity<>(responseData, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    /**
     * Reads the employee by provided id
     * @param id
     * @return
     */
    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<ResponseData> getEmployee(@PathVariable String id) {
        ResponseEntity<ResponseData> responseEntity;
        ResponseData responseData = new ResponseData();
        try {
            responseData.setStatus("success");
            responseData.setData(employeeService.getEmployee(id));
            responseData.setMessage(null);
            responseEntity = new ResponseEntity<>(responseData, HttpStatus.OK);
            LOGGER.trace("Data returned");
        } catch (Exception e) {
            LOGGER.error("Error occurred while getting data", e);
            responseData.setStatus("error");
            responseData.setData(e.getMessage());
            responseData.setMessage("Error occurred while getting data");
            responseEntity = new ResponseEntity<>(responseData, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    /**
     * Updates the employee data
     * @param employee
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT, consumes = "application/json")
    public ResponseEntity<ResponseData> updateEmployee(@RequestBody Employee employee) {
        ResponseEntity<ResponseData> responseEntity;
        ResponseData responseData = new ResponseData();
        try {
            responseData.setStatus("success");
            responseData.setData(employeeService.updateEmployee(employee));
            responseData.setMessage("Data deleted successfully");
            responseEntity = new ResponseEntity<>(responseData, HttpStatus.OK);
            LOGGER.trace("Data updated");
        } catch (Exception e) {
            LOGGER.error("Error occurred while updating data", e);
            responseData.setStatus("error");
            responseData.setData(e.getMessage());
            responseData.setMessage("Error occurred while updating data");
            responseEntity = new ResponseEntity<>(responseData, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    /**
     * Generates the billing information as per provided department
     * @param department
     * @return
     */
    @RequestMapping(path = "/billing", method = RequestMethod.GET)
    public ResponseEntity<ResponseData> generateBilling(@RequestParam(name = "department") String department) {
        ResponseEntity<ResponseData> responseEntity;
        ResponseData responseData = new ResponseData();
        try {
            responseData.setStatus("success");
            responseData.setData(employeeService.generateBilling(department));
            responseData.setMessage(null);
            responseEntity = new ResponseEntity<>(responseData, HttpStatus.OK);
            LOGGER.trace("Billing info generated");
        } catch (Exception e) {
            LOGGER.error("Error occurred while generating data", e);
            responseData.setStatus("error");
            responseData.setData(e.getMessage());
            responseData.setMessage("Error occurred while fetching billing info");
            responseEntity = new ResponseEntity<>(responseData, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }
}
